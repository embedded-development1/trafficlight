int redCrosswalkHorizontal = 2;
int greenCrosswalkHorizontal = 3;
int redCarLightHorizontal = 4;
int yellowCarLightHorizontal = 5;
int greenCarLightHorizontal = 6;
int redCarLightRow = 7;
int yellowCarLightRow = 8;
int greenCarLightRow = 9;
int greenCrosswalkRow = 10;
int redCrosswalkRow = 11;

void setup() {
  // put your setup code here, to run once:
  pinMode(redCrosswalkHorizontal, OUTPUT);
  pinMode(greenCrosswalkHorizontal, OUTPUT);
  pinMode(redCarLightHorizontal, OUTPUT);
  pinMode(yellowCarLightHorizontal, OUTPUT);
  pinMode(greenCarLightHorizontal, OUTPUT);
  pinMode(redCarLightRow, OUTPUT);
  pinMode(yellowCarLightRow, OUTPUT);
  pinMode(greenCarLightRow, OUTPUT);
  pinMode(greenCrosswalkRow, OUTPUT);
  pinMode(redCrosswalkRow, OUTPUT);
  Serial.begin(115200);
  digitalWrite(redCrosswalkHorizontal, HIGH);
  digitalWrite(greenCarLightHorizontal, HIGH);
  digitalWrite(redCarLightRow, HIGH);
  digitalWrite(greenCrosswalkRow, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(10000);
  stopHorizontalTraffic();
  delay(5000);
  startSideTraffic();
  delay(10000);
  stopSideTraffic();
  delay(5000);
  startHorizontalTraffic();
  Serial.println("Cycle has been finished");
}
void startHorizontalTraffic(){
  digitalWrite(yellowCarLightHorizontal, HIGH);
  delay(2000);
  digitalWrite(redCrosswalkRow, LOW);
  digitalWrite(greenCrosswalkRow, HIGH);
  digitalWrite(redCarLightHorizontal, LOW);
  digitalWrite(yellowCarLightHorizontal, LOW);
  digitalWrite(greenCarLightHorizontal, HIGH);
}

void stopHorizontalTraffic(){
  digitalWrite(yellowCarLightHorizontal, HIGH);
  digitalWrite(greenCarLightHorizontal, LOW);
  digitalWrite(redCrosswalkRow, HIGH);
  digitalWrite(greenCrosswalkRow, LOW);
  delay(2000);
  digitalWrite(redCarLightHorizontal, HIGH);
  digitalWrite(yellowCarLightHorizontal, LOW);
}

void startSideTraffic(){
  digitalWrite(yellowCarLightRow, HIGH);
  delay(2000);
  digitalWrite(redCrosswalkHorizontal, LOW);
  digitalWrite(greenCrosswalkHorizontal, HIGH);
  digitalWrite(redCarLightRow, LOW);
  digitalWrite(yellowCarLightRow, LOW);
  digitalWrite(greenCarLightRow, HIGH);
}

void stopSideTraffic(){
  digitalWrite(redCrosswalkHorizontal, HIGH);
  digitalWrite(greenCrosswalkHorizontal, LOW);
  digitalWrite(yellowCarLightRow, HIGH);
  digitalWrite(greenCarLightRow, LOW);
  delay(2000);
  digitalWrite(redCarLightRow, HIGH);
  digitalWrite(yellowCarLightRow, LOW);
}